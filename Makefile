SOURCEFILE=slide.adoc
SLIDE=slide
SOURCE=$(addprefix $(SLIDE)/, $(SOURCEFILE))
HTML=$(SOURCE:%.adoc=%.html)
PDF=$(HTML:%.html=%.pdf)
SIG=$(PDF:%.pdf=%.pdf.sig)
SINGLESLIDEHTML=$(PDF%:.pdf=%.SingleSlide.html)
SINGLEHTML=$(PDF%:.pdf=%.Single.html)

all: html pdf

html: $(SOURCE)
#	asciidoctor-revealjs-linux --embedded $^
	asciidoctor-revealjs-linux $^

pdf: $(HTML)
	@for FILE in $^; \
	do \
		URL="file://`pwd`/$$FILE"; \
		TMPDIR=`mktemp -d`; \
		PDF=$${FILE%.html}.pdf; \
		chromium --user-data-dir=$$TMPDIR --headless --disable-gpu --print-to-pdf="$${PDF}" "$$URL?print-pdf"; \
		JPG=$${FILE%.html}.jpg; \
		: https://matoken.org/blog/2021/03/16/make-the-first-page-of-the-pdf-file-jpeg-with-imagemagick/ \
		convert -resize 320x320 $${PDF}[0] $${JPG}; \
		rm -r $$TMPDIR; \
		gpg --yes --detach-sign $$PDF; \
	done
#	for FILE in $(HTML); do URL="file://`pwd`/$(FILE))"; echo "[${URL}]"; TMPDIR=$(mktemp -d); echo chromium --user-data-dir=$(TMPDIR) --headless --disable-gpu --print-to-pdf=${PDF} $(URL)?print-pdf; done

singlefile: $(HTML)
	rm $(SINGLEHTML) $(SINGLESLIDEHTML)
	docker run -v `pwd`/${SLIDE}:/usr/src/app/out singlefile file:///usr/src/app/out/${HTML} ${SINGLESLIDEHTML} --back-end=webdriver-chromium --remove-scripts=false
	rm ${SLIDE}/${SINGLEHTML}
	docker run -v `pwd`/${SLIDE}:/usr/src/app/out singlefile file:///usr/src/app/out/${HTML}?print-pdf ${SINGLEHTML} --back-end=webdriver-chromium

clean:
	rm $(SINGLEHTML) $(SINGLESLIDEHTML) $(SIG) $(PDF) $(HTML)

